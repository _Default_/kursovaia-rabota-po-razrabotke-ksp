package ru.mirea.petShop.balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.mirea.petShop.Balance;

import java.io.IOException;
import java.util.HashMap;

@Controller
public class BalanceController {

    @Autowired
    private BalanceService balanceService;

    @RequestMapping(value = "balance", method = RequestMethod.GET)
    @ResponseBody
    public Object getBalance(@RequestHeader HttpHeaders hh) throws IOException
    {
        HashMap<String, String> response = new HashMap<>();
        int userID = balanceService.getUserID(hh);
        response.put("Status", "Ok");
        response.put("Message", "Ваш баланс: " + Double.toString(balanceService.balance(userID).getMoney()));
        return response;
    }

    @RequestMapping(value = "balance/{userID}", method = RequestMethod.GET)
    @ResponseBody
    public Object getBalance(@PathVariable int userID, @RequestHeader HttpHeaders hh) throws IOException
    {
        String role = balanceService.getRole(hh);
        return balanceService.balance(userID, role);
    }

    @RequestMapping(value = "balance/{userID}", method = RequestMethod.POST)
    @ResponseBody
    public Object setBalance (@PathVariable int userID, @RequestBody Balance balance, @RequestHeader HttpHeaders hh) throws IOException
    {
        String role = balanceService.getRole(hh);
        Object response = balanceService.updateUserBalance(balance.getMoney(),userID, role);

        return response;
    }

    @RequestMapping(value = "error", method = RequestMethod.GET)
    @ResponseBody
    public HashMap error()
    {
        HashMap<String, String> response = new HashMap<>();
        response.put("Status", "Error 666.");
        response.put("Message", "Вы не авторизованы.");
        return response;
    }
}
