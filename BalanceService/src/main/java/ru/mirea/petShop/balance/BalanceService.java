package ru.mirea.petShop.balance;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import ru.mirea.petShop.Balance;
import ru.mirea.petShop.Token;

import java.io.IOException;
import java.util.HashMap;

@Service
public class BalanceService {

    BalanceDAO balanceDataAccessObject;

    @Autowired
    public BalanceService(BalanceDAO balanceDataAccessObject){
        this.balanceDataAccessObject = balanceDataAccessObject;
    }

    public Balance balance(int userID) {
        return balanceDataAccessObject.getBalanceByUserID(userID).get(0);
    }

    public Object balance(int userID, String role) {
        if (role.equalsIgnoreCase("admin")){
            return balanceDataAccessObject.getBalanceByUserID(userID).get(0);
        }
        HashMap<String, String> response = new HashMap<>();
        response.put("Status", "Error");
        response.put("Message", "Ты не админ сервиса.");
        return response;
    }

    public Object updateUserBalance(double newBalance, int userID, String role) {
        if (role.equalsIgnoreCase("admin")) {
            balanceDataAccessObject.updateUserBalance(newBalance, userID);
            HashMap<String, String> response = new HashMap<>();
            response.put("Status", "Ok");
            response.put("Message", "Ты изменил баланс пользователя.");
            return response;
        }
        HashMap<String, String> response = new HashMap<>();
        response.put("Status", "Error");
        response.put("Message", "Ты не админ сервиса.");
        return response;
    }

    int getUserID(HttpHeaders hh) throws IOException {
        String tokenStr = hh.get("token").get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Token token = objectMapper.readValue(tokenStr, Token.class);
        return token.getUserID();
    }
    String getRole(HttpHeaders hh) throws IOException {
        String tokenStr = hh.get("token").get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Token token = objectMapper.readValue(tokenStr, Token.class);
        return token.getRole();
    }
}
