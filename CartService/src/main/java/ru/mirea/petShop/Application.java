package ru.mirea.petShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@ServletComponentScan
@SpringBootApplication
public class Application {
    public static void main(String args[])
    {
       try {
           Connection conn = DriverManager.getConnection("jdbc:h2:~/PetShopServicesDB/CartDB", "sa", "");
           PreparedStatement ps = conn.prepareStatement("CREATE TABLE Carts (Id int PRIMARY KEY, UserId int, ItemId int)");
           ps.execute();
       }catch (SQLException e){};
       try {
           Connection conn = DriverManager.getConnection("jdbc:h2:~/PetShopServicesDB/CartDB", "sa", "");
           PreparedStatement ps = conn.prepareStatement("CREATE TABLE totalprices(userid int PRIMARY KEY, cartprice double)");
           ps.execute();
       } catch (SQLException e){};

        SpringApplication.run(Application.class);
    }
}
