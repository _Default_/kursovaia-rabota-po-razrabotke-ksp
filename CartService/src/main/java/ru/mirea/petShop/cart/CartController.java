package ru.mirea.petShop.cart;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.mirea.petShop.Cart;
import ru.mirea.petShop.Token;

import java.io.IOException;
import java.util.List;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @RequestMapping(value = "cart", method = RequestMethod.GET)
    @ResponseBody
    public Object getItems(@RequestHeader HttpHeaders hh) throws IOException {
        int userId = cartService.getUserID(hh);
        return cartService.items(userId);
    }

    @RequestMapping(value = "cart/item/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Object putPet(@PathVariable int id, @RequestHeader HttpHeaders hh) throws IOException {
        int userId = cartService.getUserID(hh);
        return cartService.putItem(id, userId);
    }

    @RequestMapping(value = "cart/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Object deleteItem(@PathVariable int id, @RequestHeader HttpHeaders hh) throws IOException {
        int userId = cartService.getUserID(hh);
        return cartService.deleteItem(id, userId);
    }

    @RequestMapping(value = "cart", method = RequestMethod.POST)
    @ResponseBody
    public Object buyItems(@RequestHeader HttpHeaders hh) throws IOException {
        int userId = cartService.getUserID(hh);
        return cartService.buy(userId);
    }
}
