package ru.mirea.petShop.cart;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.mirea.petShop.*;

import java.io.IOException;
import java.util.*;

@Service
public class CartService {

    private Map<String, String> config;
    private CartDAO cartDataAccessObject;
    private HttpHeaders headers;

    @Autowired
    public CartService(CartDAO cartDataAccessObject) throws JsonProcessingException {
        this.cartDataAccessObject = cartDataAccessObject;

        RestTemplate restTemplate = new RestTemplate();
        config = restTemplate.getForObject("http://localhost:8085/config", Map.class);

        Token token = new Token(-1, "admin");
        String signature = DigestUtils.sha256Hex(token.toString() + "LOL");
        token.setSignature(signature);
        ObjectMapper objectMapper = new ObjectMapper();
        String tokenHeader = objectMapper.writer().writeValueAsString(token);

        headers = new HttpHeaders();
        headers.add("Token" , tokenHeader);

    }


    public Object putItem(int id, int userId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<Item[]> pet = restTemplate.exchange(config.get("Item") + "/item/" + id, HttpMethod.GET, httpEntity, Item[].class);
        HashMap<String, String> response = new HashMap<>();
        if (pet.getBody().length != 0) {
            response.put("Status", "Ok");
            response.put("Message","Товар успешно добавлен в корзину.");
            cartDataAccessObject.addItem(getIndex(userId), id, pet.getBody()[0].getPrice(), userId);
            return response;
        }
        response.put("Status", "Failed");
        response.put("Message","Данного товара нет в продаже.");
        return response;
    }



    public Object deleteItem(int id, int userId)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<Item[]> pet = restTemplate.exchange(config.get("Item") + "/item/" + id, HttpMethod.GET, httpEntity, Item[].class);
        HashMap<String, String> response = new HashMap<>();
        int flag = 0;
        List<Cart> tmp = cartDataAccessObject.findAll(userId);
        for (int i = 0; i < tmp.size(); i++)
        {
            if (tmp.get(i).internalID == id)
            {
                flag = 1;
                break;
            }
        }
        if (flag == 1){
        cartDataAccessObject.deleteItem(id , userId, pet.getBody()[0].getPrice());
            response.put("Status", "Ok");
            response.put("Message","Товар успешно удален из корзины.");
            return response;
        }
        response.put("Status", "Ok");
        response.put("Message","У Вас в корзине нет товара с заданным ID.");
        return response;
    }

    public Object items(int userId) {
        List<HashMap<String, String>> response = new LinkedList<>();
        HashMap<String, String> resp = new HashMap<>();
        resp.put("Status", "Ok");
        response.add(resp);
        List<Cart> tmp = cartDataAccessObject.findAll(userId);
        if (!tmp.isEmpty()) {
            while (!tmp.isEmpty()) {
                Cart cart = tmp.remove(0);
                resp = new HashMap<>();
                resp.put("CartId", Integer.toString(cart.internalID));
                resp.put("ItemId", Integer.toString(cart.itemId));
                response.add(resp);
            }
        }
        else { resp = new HashMap<>();
            resp.put("Message", "Список покупок пуст.");
            response.add(resp);
        }
        resp = new HashMap<>();
        resp.put("Итого", Double.toString(sum(userId)));
        response.add(resp);
        return response;
    }

    public Object buy(int userId) {
        double totalPrice = cartDataAccessObject.getTotalPrice(userId);
        HashMap<String, String> response = new HashMap<>();
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<Balance> balanceResponseEntity = restTemplate.exchange(config.get("Balance") + "/balance/" + userId, HttpMethod.GET, httpEntity, Balance.class);
        Balance balance = balanceResponseEntity.getBody();

        if (balance.getMoney() < totalPrice) {
            response.put("Status", "Error 25");
            response.put("Message","У вас недостаточно денег.");
            return response;
        }

        Balance newBalance = new Balance(balance.getMoney() - totalPrice);
        HttpEntity httpEntityWithBody = new HttpEntity(newBalance, headers);
        restTemplate.exchange(config.get("Balance") + "/balance/" + userId, HttpMethod.POST, httpEntityWithBody, Balance.class);

        cartDataAccessObject.deleteAll(userId);
        response.put("Status", "Ok");
        response.put("Message","Покупка совершена.");
        return response;
    }

    int getIndex(int userId) {
        int i;
        int index = 1;
        ArrayList<Integer> arrayList = new ArrayList<>();
        int max = 1;
        for (Cart it : cartDataAccessObject.findAllUsers()) {
            if (it.internalID > max) {
                max = it.internalID;
            }
        }
        for (i = 0; i < max; i++)
            arrayList.add(i, 0);
        for (Cart it : cartDataAccessObject.findAllUsers()) {
            arrayList.set((it.internalID - 1), 1);
        }
        for (i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) != 1) {
                index = i + 1;
                break;
            }
            index = max + 1;

        }
        return index;
    }
    int getUserID(HttpHeaders hh) throws IOException {
        String tokenStr = hh.get("token").get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Token token = objectMapper.readValue(tokenStr, Token.class);
        return token.getUserID();
    }
    double sum(int userId)
    {
        return cartDataAccessObject.getTotalPrice(userId);
    }
}