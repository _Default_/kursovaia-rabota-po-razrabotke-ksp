package ru.mirea.petShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


@SpringBootApplication
public class Application {

    public static void main(String args[]) {
        try
        {
            Connection conn = DriverManager.getConnection("jdbc:h2:~/PetShopServicesDB/UserDB", "sa", "");
            //org.h2.tools.Server.startWebServer(conn);
            PreparedStatement ps = conn.prepareStatement("CREATE TABLE Users (USERID int PRIMARY KEY auto_increment, login varchar, role varchar, password varchar)");
            ps.execute();
            ps = conn.prepareStatement("INSERT INTO Users(USERid, login, role, password) VALUES (1, 'Alexey', 'admin','12345')");
            ps.execute();
        }catch(SQLException e){}
        SpringApplication.run(Application.class);

    }
}