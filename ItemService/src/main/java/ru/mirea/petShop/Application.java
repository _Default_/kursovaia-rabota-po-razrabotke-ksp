package ru.mirea.petShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@ServletComponentScan
@SpringBootApplication
public class Application {

    public static void main(String args[]) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:h2:~/PetShopServicesDB/ItemDB", "sa", "");
            PreparedStatement ps = conn.prepareStatement("CREATE TABLE Items (Id int PRIMARY KEY, Name VARCHAR, Price double, IsPet bit, Age double)");
            ps.execute();
        }catch (SQLException e){}
        SpringApplication.run(Application.class);
    }
}
