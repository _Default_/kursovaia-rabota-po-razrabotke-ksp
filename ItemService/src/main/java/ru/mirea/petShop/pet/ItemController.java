package ru.mirea.petShop.pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.mirea.petShop.Item;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value ="pet", method = RequestMethod.GET)
    @ResponseBody
    public Object getPets(){
        return itemService.findAllPet();
    }

    @RequestMapping(value = "staff", method = RequestMethod.GET)
    @ResponseBody
    public Object findStaffs() {
        return itemService.findAllStaff();
    }

    @RequestMapping(value ="item/", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getItems(){
        return itemService.findAllItem();
    }

    @RequestMapping(value = "item/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> findItems(@PathVariable int id)
    {
        return itemService.findItemByID(id);
    }

    @RequestMapping(value="items/{item}", method = RequestMethod.PUT)
    @ResponseBody
    public Object addItem(@PathVariable String item,@RequestHeader HttpHeaders hh) throws IOException
    {
        if (!itemService.getRole(hh).equalsIgnoreCase("admin"))
        {
            HashMap<String, String> response = new HashMap<>();
            response.put("Status","Error! Error! Error!");
            response.put("Message", "А ну убери отсюда свои шелудивые ручонки!!!");
            return response;
        }
        else {
        String[] tmp = item.split(" "); //  name, type, age, cost
        Item elem;
        if (tmp[1].equals("Pet")) {
            elem = new Item(1, Double.parseDouble(tmp[3]), 1, tmp[0], Double.parseDouble(tmp[2]));
        }
        else if (tmp[1].equals("Staff")) {
            elem = new Item(1, Double.parseDouble(tmp[3]), 0, tmp[0], Double.parseDouble(tmp[2]));
        }
        else elem = null;
        itemService.addItem(elem);
        HashMap<String, String> response = new HashMap<>();
        response.put("Status","Запрос выполнен.");
        response.put("Message", "Товар добавлен в продажу.");
        return response;
    }
    }
    @RequestMapping(value="items/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Object removeItems(@PathVariable("id") String id, @RequestHeader HttpHeaders hh) throws IOException {
        HashMap<String, String> response = new HashMap<>();
        if (itemService.getRole(hh).equalsIgnoreCase("admin")){
        List<Item> item = itemService.findItemByID(Integer.parseInt(id));
        if (!item.isEmpty()){
        boolean wasOK = itemService.removeItem(item.get(0));
        if (wasOK){
            response.put("Status", "Запрос выполнен");
            response.put("Response", "Товар убран из продажи.");
            return response;
        }
        response.put("Status", "Error 100");
        response.put("Response", "Невозможно удалить товар.");
        return response;
    }
        response.put("Status", "Error 101");
        response.put("Response", "Товара с данным ID нет в продаже.");
        return response;
    }
    else {
            response.put("Status","Error 102.");
            response.put("Message", "Не влезай! Убьет!");
            return response;}
    }

    @RequestMapping(value = "err", method = RequestMethod.GET)
    @ResponseBody
    public HashMap error()
    {
        HashMap<String, String> response = new HashMap<>();
        response.put("Status", "Error 666.");
        response.put("Message", "Вы не авторизованы.");
        return response;
    }
}
