package ru.mirea.petShop.pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import ru.mirea.petShop.Item;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class ItemDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ItemDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Item> findAllItem() {
        return jdbcTemplate.query("select * from Items", new ItemRowMapper());
    }
    public List<Item> findItemByID(int id) {
        return jdbcTemplate.query("select * from Items where Id =" + id , new ItemRowMapper());
    }
    public List<Item> findAllPet() {
        return jdbcTemplate.query("select Id, Name, Price , Age from Items where isPet = 1", new PetRowMapper());
    }

    public List<Item> findPetByID(int id) {
        return jdbcTemplate.query("select Id, Name, Price, Age from Items where isPet = 1 and Id =" + id , new PetRowMapper());
    }
   @Nullable
    public List<Item> findAllStaff() {
        return jdbcTemplate.query("select * from items  where isPet = 0", new StaffRowMapper());
    }

    @Nullable
    public List<Item> findStaffByID(int id) {
        return jdbcTemplate.query("select Id, Name, Price from Items where isPet = 0 and Id =" + id , new StaffRowMapper());
    }
    public void addItem(Item item)
    {
        jdbcTemplate.execute("INSERT INTO Items(Id, Name, Price, IsPet, Age) VALUES (" + item.getID() + " ,'" + item.getName() + "', "+ item.getPrice() + ","+ item.getIsPet() + "," + item.getAge()+")");
    }
    public void removeItem(Item item)
    {
        jdbcTemplate.execute("Delete from Items where Id = " + item.getID());
    }
    class StaffRowMapper implements RowMapper<Item>{
        @Override
        public Item mapRow(ResultSet resultSet, int i) throws SQLException {
            int id = resultSet.getInt("Id");
            String staffName = resultSet.getString("Name");
            double price = resultSet.getDouble("price");
            double age = 0;
            return new Item(id, price, 0, staffName, age);
        }
    }

    private class PetRowMapper implements RowMapper<Item> {

        @Override
        public Item mapRow(ResultSet resultSet, int i) throws SQLException {
            int id = resultSet.getInt("Id");
            String petName = resultSet.getString("Name");
            double price = resultSet.getDouble("price");
            double age = resultSet.getDouble("Age");

            return new Item(id, price, 1, petName, age);
        }
    }

        private class ItemRowMapper implements RowMapper<Item> {

            @Override
            public Item mapRow(ResultSet resultSet, int i) throws SQLException {
                int id = resultSet.getInt("Id");
                String petName = resultSet.getString("Name");
                int isPet = resultSet.getInt("isPet");
                double price = resultSet.getDouble("price");
                double age = resultSet.getDouble("Age");

                return new Item(id, price, isPet, petName, age);
            }
    }
}
