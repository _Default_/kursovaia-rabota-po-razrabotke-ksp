
package ru.mirea.petShop.pet;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import ru.mirea.petShop.Item;
import ru.mirea.petShop.Token;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ItemService {
    private int index = 1;
    ItemDAO itemDAO;

    @Autowired
    public ItemService(ItemDAO petDataAcsesObject)
    {
        this.itemDAO = petDataAcsesObject;
    }

    @Nullable
    public Object findAllPet()
    {
        List<Item> tmp = itemDAO.findAllPet();
        if (tmp.isEmpty())
        {
            HashMap<String, String> response = new HashMap<>();
            response.put("Status", "Запрос выполнен.");
            response.put("Message","Товаров данного типа в продаже нет.");
            return response;
        }
        return tmp;
    }

    @Nullable
    public  Object findAllStaff()
    {
        List<Item> tmp = itemDAO.findAllStaff();
        if (tmp.isEmpty())
        {
            HashMap<String, String> response = new HashMap<>();
            response.put("Status", "Запрос выполнен.");
            response.put("Message","Товаров данного типа в продаже нет.");
            return response;
        }
        return tmp;
    }

    @Nullable
    public  List<Item> findAllItem() {
        return  itemDAO.findAllItem();
    }

    @Nullable
    public List<Item> findItemByID(int id)
    {
        return  itemDAO.findItemByID(id);
    }

    public boolean addItem(Item item) {
        int i;
        ArrayList<Integer> arrayList= new ArrayList<>();
        int max = 0;
        for (Item it: findAllItem())
        {
            if (it.getID()>max)
            {
                max = it.getID();
            }
        }
        for (i = 0; i < max; i++)
            arrayList.add(i, 0);
        for (Item it: findAllItem())
        {
            arrayList.set((it.getID()-1), 1);
        }
        for (i = 0; i < arrayList.size(); i++){
            if (arrayList.get(i)!= 1) {
                index = i + 1;
                break;
            }
            index = max + 1;
            item.setId(index);
        }

        itemDAO.addItem(item);
        return true;
    }
    public boolean removeItem(Item item) {
        itemDAO.removeItem(item);
        return true;
    }




    int getUserID(HttpHeaders hh) throws IOException {
        String tokenStr = hh.get("token").get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Token token = objectMapper.readValue(tokenStr, Token.class);
        return token.getUserID();
    }
    String getRole(HttpHeaders hh) throws IOException {
        String tokenStr = hh.get("token").get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Token token = objectMapper.readValue(tokenStr, Token.class);
        return token.getRole();
    }
}



