package ru.mirea.petShop;

public class Cart {
    public int internalID;
    public int userId;
    public int itemId;

    public Cart(int internalID, int userId, int itemId){
        this.internalID = internalID;
        this.userId = userId;
        this.itemId = itemId;
    }
}
